package urma.chap2;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Adam on 7/14/2015.
 */
public class LVCExample {
    public static void main(String[] args) {

        List<Integer> intSeq = Arrays.asList(1, 2, 3);

        final int var = 10; //must be final or effectively final
        intSeq.forEach(x -> System.out.println(x + var));
    }
}