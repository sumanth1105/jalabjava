package urma.practice;

public class Transaction{

	private Trader trader;
	private int year;
	private int value;
	private String currency;

	public Transaction(Trader trader, int year, int value)
	{
		this.trader = trader;
		this.year = year;
		this.value = value;
	}

	public Transaction(Trader trader, int year, int value, String currency)
	{
		this.trader = trader;
		this.year = year;
		this.value = value;
	}


	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Trader getTrader(){
		return this.trader;
	}

	public int getYear(){
		return this.year;
	}

	public int getValue(){
		return this.value;
	}
	
	public String toString(){
	    return "{" + this.trader + ", " +
	           "year: "+this.year+", " +
	           "value:" + this.value +"}";
	}
}